$(function () {
    new WOW({ offset: 150 }).init()
    // 點擊a滑動到指定位置
    $('a[href^="#"]').on('click', function (e) {
        e.preventDefault()
        let target = $(this).attr('href')
        let targetPos = $(target).offset().top
        $('html, body').animate({ scrollTop: targetPos }, 1000)
    })

    //輪播
    const conceptSlider1 = document.querySelectorAll('.product-24A-16 .concept-product-slide')
    conceptSlider1.forEach(function (container) {
        const spSwiperC1 = new Swiper(container.querySelector('.swiper'), {
            slidesPerView: 1,
            spaceBetween: 30,
            centeredSlides: true,
            loop: true,
            autoplay: {
                delay: 5000,
                disableOnInteraction: false,
            },
            navigation: {
                nextEl: container.querySelector('.custom-swiper-button-next'),
                prevEl: container.querySelector('.custom-swiper-button-prev'),
            },
            breakpoints: {
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 20,
                    autoplay: false,
                    allowTouchMove: false,
                    centeredSlides: false,
                },
            },
        })
    })

    const conceptSlider2 = document.querySelectorAll('.product-24A-15 .concept-product-slide')
    conceptSlider2.forEach(function (container) {
        const spSwiperC2 = new Swiper(container.querySelector('.swiper'), {
            slidesPerView: 1,
            spaceBetween: 30,
            centeredSlides: true,
            loop: true,
            autoplay: {
                delay: 5000,
                disableOnInteraction: false,
            },
            navigation: {
                nextEl: container.querySelector('.custom-swiper-button-next'),
                prevEl: container.querySelector('.custom-swiper-button-prev'),
            },
            breakpoints: {
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 20,
                    autoplay: false,
                    allowTouchMove: false,
                    centeredSlides: false,
                },
            },
        })
    })

    const scenesSlider = document.querySelectorAll('.scenes-product-slide')
    scenesSlider.forEach(function (container) {
        const spSwiper2 = new Swiper(container.querySelector('.swiper'), {
            slidesPerView: 1,
            spaceBetween: 0,
            loop: true,
            centeredSlides: true,
            autoplay: {
                delay: 5000,
                disableOnInteraction: false,
            },
            navigation: {
                nextEl: container.querySelector('.custom-swiper-button-next'),
                prevEl: container.querySelector('.custom-swiper-button-prev'),
            },
            breakpoints: {
                1024: {
                    slidesPerView: 2,
                    centeredSlides: false,
                    autoplay: false,
                    allowTouchMove: false,
                },
            },
        })
    })

    let giveawaySlider = new Swiper('.giveaway-slider', {
        slidesPerView: 1,
        spaceBetween: 10,
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.giveaway .pic .custom-swiper-button-next',
            prevEl: '.giveaway .pic .custom-swiper-button-prev',
        },
    })

    let thumbsSlider = new Swiper('.itemPanel-1 .thumbnail-slider', {
        spaceBetween: 10,
        slidesPerView: 1,
        loop: true,
        freeMode: false,
        navigation: {
            nextEl: '.itemPanel-1 .thumbnail .custom-swiper-button-next',
            prevEl: '.itemPanel-1 .thumbnail .custom-swiper-button-prev',
        },
        breakpoints: {
            768: {
                slidesPerView: 2,
                spaceBetween: 10,
                loop: true,
            },
        },
    })

    let gallerySlider = new Swiper('.itemPanel-1 .gallery-slider', {
        spaceBetween: 0,
        loop: true,
        // thumbs: {
        //     swiper: thumbsSlider,
        // },
    })

    let isGallerySliderChanging = false
    let isThumbsSliderChanging = false

    gallerySlider.on('slideChangeTransitionStart', function () {
        if (!isThumbsSliderChanging) {
            isGallerySliderChanging = true
            thumbsSlider.slideTo(gallerySlider.activeIndex)
        }
        isGallerySliderChanging = false
    })

    thumbsSlider.on('transitionStart', function () {
        if (!isGallerySliderChanging) {
            isThumbsSliderChanging = true
            gallerySlider.slideTo(thumbsSlider.activeIndex)
        }
        isThumbsSliderChanging = false
    })

    let thumbsSlider2 = new Swiper('.itemPanel-2 .thumbnail-slider', {
        spaceBetween: 10,
        slidesPerView: 1,
        loop: true,
        freeMode: false,
        navigation: {
            nextEl: '.itemPanel-2 .thumbnail .custom-swiper-button-next',
            prevEl: '.itemPanel-2 .thumbnail .custom-swiper-button-prev',
        },
        breakpoints: {
            768: {
                slidesPerView: 2,
                spaceBetween: 10,
                loop: true,
            },
        },
    })

    let gallerySlider2 = new Swiper('.itemPanel-2 .gallery-slider', {
        spaceBetween: 0,
        loop: true,
        // thumbs: {
        //     swiper: thumbsSlider2,
        // },
    })

    // thumbsSlider.controller.control = gallerySlider
    // gallerySlider.controller.control = thumbsSlider
    // thumbsSlider2.controller.control = gallerySlider2
    // gallerySlider2.controller.control = thumbsSlider2

    var isGallerySlider2Changing = false
    var isThumbsSlider2Changing = false

    gallerySlider2.on('slideChangeTransitionStart', function () {
        if (!isThumbsSlider2Changing) {
            isGallerySlider2Changing = true
            thumbsSlider2.slideTo(gallerySlider2.activeIndex)
        }
        isGallerySlider2Changing = false
    })

    thumbsSlider2.on('transitionStart', function () {
        if (!isGallerySlider2Changing) {
            isThumbsSlider2Changing = true
            gallerySlider2.slideTo(thumbsSlider2.activeIndex)
        }
        isThumbsSlider2Changing = false
    })
})
